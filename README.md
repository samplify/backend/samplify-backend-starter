# Fhir Server Starter

## Description
Quickstart guide for Samplify backend installation.

## Usage

### Docker Compose

1. Obtain [project](https://gitlab.com/samplify/backend/samplify-backend-starter.git) files:
```shell
git clone https://gitlab.com/samplify/backend/samplify-backend-starter.git
cd samplify-backend-starter
```

2. Run services:
```shell
docker compose up -d
```

### Java

1. Install OpenJDK 17 or later
2. Obtain application code:
```shell
#TODO
#wget 'https://<username>:<password>@gitlab.com/samplify/backend/fhir-server/-/package_files/43795876/download' -O './fhir-server/app.war'
cd './fhir-server'
```
3. Run app:
```shell
java -jar app.war
```

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

- [ ] add database init script
- [ ] provide multi-tenant configuration
